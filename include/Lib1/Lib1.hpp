#pragma once

/**
 * @file Lib1.hpp
 *
 */

#include <string>

/**
 * @brief hello
 *
 * @return std::string
 */
std::string hello();

/**
 * @brief world
 *
 * @return std::string
 */
std::string world();
