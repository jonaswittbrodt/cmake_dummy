# --------------- set a default build type if none was specified --------------
# defaults to Release
set(default_build_type "Release")
# unless we are in a git checkout of something other than the master
if(EXISTS "${CMAKE_SOURCE_DIR}/.git")
  file(READ "${CMAKE_CURRENT_SOURCE_DIR}/.git/HEAD" GIT_HEAD)
  string(REGEX MATCH
               ".*/master$"
               ON_MASTER
               ${GIT_HEAD})
  if(NOT ON_MASTER)
    set(default_build_type "Debug")
  endif(NOT ON_MASTER)
endif()

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(
    STATUS
      "Setting build type to '${default_build_type}' as none was specified.")
  set(CMAKE_BUILD_TYPE "${default_build_type}"
      CACHE STRING "Choose the type of build."
      FORCE)
  # Set the possible values of build type
  set_property(CACHE CMAKE_BUILD_TYPE
               PROPERTY STRINGS
                        "Debug"
                        "Release"
                        "MinSizeRel"
                        "RelWithDebInfo")
endif()
