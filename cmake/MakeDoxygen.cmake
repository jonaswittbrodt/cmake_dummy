find_package(Doxygen)

if(DOXYGEN_FOUND)
		configure_file(${CMAKE_SOURCE_DIR}/doc/Doxyfile.in Doxyfile @ONLY)
		add_custom_target(doc
			COMMAND ${DOXYGEN_EXECUTABLE} ${doxyfile}
			WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
			COMMENT "Generating Documentation with Doxygen")
endif()
