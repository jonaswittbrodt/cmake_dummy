# Cmake Example Project

Dummy CMake `c++` project.

Currently includes:

  - default build type depending on git branch
  - documentation with [Doxygen](http://www.doxygen.nl/) with CMake integration
  - testing using [Catch2](https://github.com/catchorg/Catch2) and CTest
  - subdirectory and CMake package export
